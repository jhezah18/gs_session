<?php

    class MyStore{
        private $server = "mysql:host=localhost;dbname=ortiz_gs";
        private $user="root";
        private $pass="";
        private $options=array(PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    );
    
        protected $connection;
    
        public function openConnection(){
            try{
                $this->connection=new PDO(
                    $this->server,
                    $this->user,
                    $this->pass,
                    $this->options,
    
                );
                echo "Connection success";
    
                return $this->connection;
    
            }catch(PDOException $error){
                echo "Error Connection: ".$error->getMessage();
            }
        }
        
        public function closeConnection(){
            $this->connection=null;
        }

        public function getUsers(){
            $connection = $this->openConnection();
            $statement = $connection->prepare("SELECT * FROM user");
            $statement->execute();
            $user = $statement->fetchAll();
            $usersCount = $statement->rowCount();

        if($usersCount >0){
            return $user;
        }else{
            return 0;
}
        } //End of getUser function
        // LogIn Function
            public function login(){
                if(isset($_POST['submit'])){
                     
                            $username=$_POST['username'];
                            $password=$_POST['psw'];
                            $connection=$this->openConnection();
                            $statement=$connection->prepare("SELECT * FROM user  where User_Name=? AND Password=?");
                            $statement->execute([$username,$password]);
                            $total=$statement->rowCount();
                        
                            if ($total >0){
                                
                                header('location:landing.php');
                                }else{
                                // header("location: login.php?error=Your account doesn't exist");
                                //         exit();
                                     echo'<script>alert("Make sure to provide your details correctly")</script>';
                                }
                        
                }
            }
    }
    
$myStore= new Mystore();

?>

